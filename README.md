#Simple React Weather App
###Vorschau


![Alt text](https://www.bilder-upload.eu/thumb/9fb758-1542225450.png "Weather App")

[Hier Gehts zum Vorschaubild](https://www.bilder-upload.eu/bild-9fb758-1542225450.png.html)

[Hier gehts zur Live Demo](https://build-4bpb5f1xd.now.sh/)


A simple React weather app. 
 
There you can see the weather data of all countries. 
 
Just fill in both fields and click on the button.

## Quick Start
``` bash 

# Install all dependencies for React
npm install

# Start the app
npm start

# Build the Project
npm run build
```